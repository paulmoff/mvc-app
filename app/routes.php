<?php
use Pecee\SimpleRouter\SimpleRouter;

SimpleRouter::get('/', 'HomeController@index')->name('home');
SimpleRouter::get('/pages', 'ParsedPagesController@index')->name('results');
SimpleRouter::get('/pages/{id}', 'ParsedPagesController@show')->name('page');
SimpleRouter::post('/parser', 'ParserController@parse');