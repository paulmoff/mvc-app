<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParsedPage extends Model
{
    protected $table = 'parsed_pages';
    public $timestamps = false;

    protected $fillable = [
        'quantity',
        'elements',
        'url'
    ];
}