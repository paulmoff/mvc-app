<?php
namespace App\Services;

use App\Exceptions\ParserException;
use App\Models\ParsedPage;
use Symfony\Component\HttpFoundation\Request;

class ParserService implements ParserInterface
{
    private $validation_messages = [];

    /**
     * Parse page by provided URL
     *
     * @param Request $request
     * @return array|bool
     * @throws ParserException
     */
    public function parse(Request $request) {

        if ( ! $this->validate($request)) return $this->validation_messages;

        switch ($request->get('searchType')) {
            case 'Images':
                $expression = '/<img[^>]+>/';
                break;
            case 'Links':
                $expression = '/<a[^>]+>/';
                break;
            case 'Text':
                $expression = '/'. $request->get('searchText') .'/';
                break;
            default:
                throw new ParserException('Parser search type is not defined', 0);
        }

        try {
            $ch = curl_init($request->get('pageUrl'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
            $html = curl_exec($ch);
            $httpcode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);

            if ($httpcode !== 200) throw new ParserException('Bad URL failure', 0);

            curl_close($ch);
        }  catch (ParserException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw new ParserException("Failed to pull html from URL", 0, $e);
        }

        preg_match_all($expression, $html, $matches);

        $data = [
            'quantity' => count($matches[0]),
            'elements' => implode(' ', $matches[0]),
            'url' => $request->get('pageUrl')
        ];

        // save to database
        $parsed_page = new ParsedPage();

        try {
            $parsed_page->fill($data);
        } catch (\Exception $e) {
            throw new ParserException('Attributes assignment failure', 0, $e);
        }

        try {
            $parsed_page->save();
        } catch (\Exception $e) {
            var_dump($e);
            throw new ParserException($e->getMessage(), 0, $e);
        }

        return true;
    }

    /**
     * Validate request
     *
     * @param Request $request
     * @return bool
     */
    private function validate(Request $request) {

        $values = $request->request->all();

        $rules = [
            'pageUrl' => ['required', 'url', 'unique:parsed_pages,url'],
        ];

        $messages = array(
            'pageUrl.required' => 'Page URL is required.',
            'pageUrl.unique' => 'This URL is already parsed',
            'pageUrl.url' => 'Page URL must contain a valid URL',
            'searchText.required' => 'Search text is required.',
            'searchText.max' => 'Search text should not be longer than :max symbols.',
            'searchText.min' => 'Search text should not be shorter than :min symbols.',
            'searchType.required' => 'Search type is required.',
        );

        if ($request->get('searchType') === 'Text')
            $rules['searchText'] = ['required','max:255', 'min:3'];

        $validator = (new ValidatorService())->getValidator($values, $rules, $messages);

        if ($validator->fails()) {
            $this->validation_messages = $validator->messages();
            return false;
        }

        return true;
    }
}