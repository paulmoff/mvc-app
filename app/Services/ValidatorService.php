<?php
namespace App\Services;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Translation\Translator;
use Illuminate\Translation\FileLoader;
use Illuminate\Validation\Factory as ValidatorFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\DatabasePresenceVerifier;

class ValidatorService
{
    protected $presenceResolver;
    protected $factory;

    public function __construct()
    {
        $filesystem = new Filesystem();
        $fileLoader = new FileLoader($filesystem, '');
        $translator = new Translator($fileLoader, 'en_US');
        $factory = new ValidatorFactory($translator);
        $connectionResolver = Model::getConnectionResolver();
        $presenceResolver = new DatabasePresenceVerifier($connectionResolver);

        $this->presenceResolver = $presenceResolver;
        $this->factory = $factory;
    }

    /**
     * Get validator instance
     *
     * @param $values
     * @param $rules
     * @param $messages
     * @return \Illuminate\Validation\Validator
     */
    public function getValidator($values, $rules, $messages) {
        $validator = $this->factory->make($values, $rules, $messages);
        $validator->setPresenceVerifier($this->presenceResolver);

        return $validator;
    }
}