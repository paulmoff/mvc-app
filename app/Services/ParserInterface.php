<?php
namespace App\Services;

use Symfony\Component\HttpFoundation\Request;

interface ParserInterface
{
    public function parse(Request $request);
}