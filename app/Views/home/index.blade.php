@extends('layouts.app')

@section('content')

    <div id="app" class="mx-auto" v-cloak>
        <h2 class="page-header">Search elements</h2>

        <p><a href="{{ url('results') }}" class="btn btn-info">Search results</a></p>
        <hr>

        <div v-show="bag.errors.length > 0">

            <div v-for="(error, index) in bag.errors" class="alert alert-danger" role="alert">
                @{{ error }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close" v-on:click="flushError(index)">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>

        <div v-show="bag.success.length > 0">

            <div v-for="(success, index) in bag.success" class="alert alert-success" role="alert">
                @{{ success }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close" v-on:click="flushSuccess(index)">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>

        <div class="form-group">
            <label for="pageUrl">Page URL</label>
            <input v-model="pageUrl" name="pageUrl" type="text" class="form-control" id="pageUrl" placeholder="Enter URL" v-validate="'required|url:true'">
            <span v-show="errors.has('pageUrl')" class="text-danger">@{{ errors.first('pageUrl') }}</span>
        </div>
        <div class="form-group">
            <label for="searchType">Select search target</label>
            <select v-model="searchType" class="form-control" id="searchType" v-validate="'required'">
                <option>Images</option>
                <option>Links</option>
                <option>Text</option>
            </select>
        </div>
        <div class="form-group" v-if="searchType == 'Text'">
            <label for="searchText">Text</label>
            <input v-model="searchText" name="searchText" type="text" class="form-control" id="searchText" placeholder="Enter text to search" v-validate="'required|max:255|min:3'">
            <span v-show="errors.has('searchText')" class="text-danger">@{{ errors.first('searchText') }}</span>
        </div>
        <button v-on:click="search" type="submit" class="btn btn-primary" v-show="!bag.pending">Search</button>
        <div class="progress" v-show="bag.pending">
            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
        </div>

    </div>
@endsection

@section('scripts')
    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    <script src="https://unpkg.com/vue"></script>
    <script src="/js/vee-validate.min.js"></script>
    <script src="/js/script.js"></script>
@endsection