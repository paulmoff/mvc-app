@extends('layouts.app')

@section('content')
    <div id="app" class="mx-auto">
        <h2 class="page-header">Parsed pages</h2>
        <p><a href="{{ url('home') }}" class="btn btn-info">Home</a></p>
        @if(count($pages) > 0)
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Page URL</th>
                        <th scope="col">Quantity</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($pages as $page)
                        <tr>
                            <td>{{ $page->id }}</td>
                            <td><a href="{{ url('page', ['id' => $page->id]) }}">{{ $page->url }}</a></td>
                            <td>{{ $page->quantity }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <p>Nothing parsed so far.</p>
        @endif
    </div>
@endsection