@extends('layouts.app')

@section('content')
    <div id="app" class="mx-auto">
        <h2 class="page-header">Parsed page</h2>
        <p>
            <a href="{{ url('home') }}" class="btn btn-info">Home</a>
            <a href="{{ url('results') }}" class="btn btn-info">Parsed pages</a>
        </p>
        <hr>
        <p>{{ $page->url }}</p>
        <code>{{ $page->elements }}</code>
    </div>
@endsection