<?php
namespace App\Controllers;

use Core\Controller;

class HomeController extends Controller
{
    /**
     * Show home page
     */
    public function index() {
        echo view('home.index');
    }
}