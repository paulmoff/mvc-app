<?php
namespace App\Controllers;

use App\Services\ParserService;
use Core\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ParserController extends Controller
{
    private $parserService;

    public function __construct()
    {
        // TODO refactor this to be injected via container
        $this->parserService = new ParserService();
    }

    /**
     * Parse page by provided URL
     */
    public function parse() {
        $request = Request::createFromGlobals();

        try {
            $result = $this->parserService->parse($request);
        } catch (\Exception $exception) {
            $response = new Response(
                json_encode([$exception->getMessage()]),
                Response::HTTP_BAD_REQUEST,
                array('content-type' => 'application/json')
            );

            $response->send();
            return;
        }

        // if validation failed we'll receive MessageBag
        if (is_object($result)) {
            $response = new Response(
                json_encode($result->all()),
                Response::HTTP_BAD_REQUEST,
                array('content-type' => 'application/json')
            );
        } else {
            $response = new Response(
                json_encode(['Successfully parsed!']),
                Response::HTTP_OK,
                array('content-type' => 'application/json')
            );
        }

        $response->send();
    }
}