<?php
namespace App\Controllers;

use App\Models\ParsedPage;
use Core\Controller;

class ParsedPagesController extends Controller
{
    /**
     * Show a list of parsed pages
     */
    public function index() {
        $pages = ParsedPage::all();
        echo view('results.index', compact('pages'));
    }

    /**
     * Show parsed page
     *
     * @param $id
     */
    public function show($id) {
        $page = ParsedPage::findOrFail($id);
        echo view('results.show', compact('page'));
    }
}