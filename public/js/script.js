$(function(){

    // validation config
    const config = {
        errorBagName: 'errors', // change if property conflicts
        fieldsBagName: 'fields',
        delay: 10,
        locale: 'en',
        dictionary: null,
        strict: true,
        classes: false,
        classNames: {
            touched: 'touched', // the control has been blurred
            untouched: 'untouched', // the control hasn't been blurred
            valid: 'valid', // model is valid
            invalid: 'is-invalid', // model is invalid
            pristine: 'pristine', // control has not been interacted with
            dirty: 'dirty' // control has been interacted with
        },
        events: 'blur',
        inject: true,
        validity: false,
        aria: true
    };

    Vue.use(VeeValidate, config);

    var app = new Vue({
        el: '#app',
        data: {
            searchType: 'Links',
            searchText: '',
            pageUrl: '',
            bag: {
                errors: [],
                success: [],
                pending: false
            }
        },
        methods: {
            flushError: function(event, index) {
                this.bag.errors.splice(index, 1);
            },
            flushSuccess: function(event, index) {
                this.bag.success.splice(index, 1);
            },
            search: function(event) {
                var data = {
                    searchType: this.searchType,
                    searchText: this.searchText,
                    pageUrl: this.pageUrl
                };

                var bag = this.bag;

                this.$validator.validateAll().then(function(result) {
                    if (result) {
                        bag.errors = [];
                        bag.success = [];

                        $.ajax('/parser', {
                            data: data,
                            method: 'POST',
                            success: function(data, textStatus) {
                                bag.success = bag.success.concat(data);
                            },
                            error: function(xhr, textStatus) {
                                bag.errors = bag.errors.concat(xhr.responseJSON);
                            },
                            beforeSend: function() {
                                bag.pending = true;
                            },
                            complete: function() {
                                bag.pending = false;
                            }
                        });
                    }
                });
            }
        },
        watch: {
            searchType: function (searchType) {
                if (searchType !== 'Text') {
                    this.searchText = '';
                }
            }
        }
    });
});