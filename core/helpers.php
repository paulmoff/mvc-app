<?php
function base_url($url) {
    $result = parse_url($url);
    return $result['scheme']."://".$result['host'];
}