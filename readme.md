## Installation

You will need Composer installed globally, otherwise you will have to run commands from install script one-by-one.

Run command `./install` or `bash install`

Enter MySQL-database credentials in `.env` file

Run command `vendor/bin/phinx migrate` to run migrations

## Server

You can use local PHP server: cd into public directory `cd public` and run `php -S localhost:8080`

Navigate to http://localhost:8080 to see application

#### Apache

Enable mod_rewrite and configure virtual host to have web-root in `public/` folder of the app.

#### nginx

Configure Nginx with proper location

```
location / {
    try_files $uri $uri/ /index.php?$query_string;
}
```

# Описание задания

Написать поисковый модуль, который находит элементы на главных страницах сторонних сайтов (например, ссылки или картинки).
Модуль должен быть написан как полноценное php-приложение с использованием концепции MVC.

На главной странице изначально выводится форма ввода адреса сайта и выбора типа поиска (ссылки, изображения, текст).
При выборе «текст» появляется поле ввода, при выборе другого пункта исчезает.

Данные отправляются на сервер AJAX-запросом. Валидация всех полей должна быть как на стороне клиента, так и на стороне сервера, сообщения об ошибке валидации выводить без перезагрузки страницы.

Скрипт должен искать на указанной странице сайта (страница скачивается с помощью CURL или другим способом) выбранные элементы, используя регулярные выражения.
Адрес сайта, найденные элементы (одной строкой) и число найденных элементов записывать в БД (PostgreSQL или MySQL).

Страница просмотра результатов – список проверенных сайтов и числа найденных элементов. По клику на адрес сайта осуществляется вывод найденных значений.

Шаблонизатор использовать любой, нельзя использовать PHP-фреймворки. Можно использовать jQuery, MooTools, Sencha или аналоги.